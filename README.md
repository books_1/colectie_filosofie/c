# C

## Content

```
./C. Radulescu Motru:
C. Radulescu Motru - Elemente de metafizica.pdf

./Carl Gustav Jung:
Carl Gustav Jung - Puterea sufletului (Antologie I).pdf
Carl Gustav Jung - Puterea sufletului (Antologie II).pdf
Carl Gustav Jung - Puterea sufletului (Antologie III).pdf
Carl Gustav Jung - Puterea sufletului (Antologie IV).pdf

./Carl Sagan:
Carl Sagan - Balaurii Raiului, consideratii asupra evolutiei inteligentei umane.pdf

./Catalin Cioaba:
Catalin Cioaba - Jocul cu timpul.pdf
Catalin Cioaba - Timp si temporalitate.pdf

./Catalin Zamfir:
Catalin Zamfir - Dictionar de sociologie.pdf

./Cercetari:
Cercetari filosofico-psihologice anul I 1.pdf
Cercetari filosofico-psihologice anul II 1.pdf
Cercetari filosofico-psihologice anul II 2.pdf
Cercetari filosofico-psihologice anul III 1.pdf
Cercetari filosofico-psihologice anul III 2.pdf
Cercetari filosofico-psihologice anul IV 1.pdf
Cercetari filosofico-psihologice anul IV 2.pdf
Cercetari filosofico-psihologice anul V 1.pdf
Cercetari filosofico-psihologice anul V 2.pdf

./Chaim Perelman:
Chaim Perelman - Tratat de argumentare.pdf

./Charles Darwin:
Charles Darwin - Originea Speciilor.pdf

./Charles Sanders Peirce:
Charles Sanders Peirce - Semnificatie si actiune.pdf

./Christopher Janaway:
Christopher Janaway - Schopenhauer (Maestrii spiritului).pdf

./Cicero:
Cicero - Despre divinatie.pdf

./Claude Levi Strauss:
Claude Levi Strauss - Antropologia structurala.pdf

./Claudiu Mesaros:
Claudiu Mesaros - Dialoguri cu Platon.pdf
Claudiu Mesaros - Filosofii cerului (O introducere critica in gandirea Evului mediu).pdf
Claudiu Mesaros - Introducere in filosofia antica (Presocraticii).pdf

./Comentarii:
Comentarii la Tratatul 'Despre Interpretare' al lui Aristotel.pdf

./Confucius:
Confucius - Analecte.pdf

./Constantin Aslam:
Constantin Aslam - Constantin Noica (Spre un model neoclasic de gandire).pdf

./Constantin Enachescu:
Constantin Enachescu - Tratat de psihanaliza si psihoterapie.pdf

./Constantin Grecu & Iancu Lucica:
Constantin Grecu & Iancu Lucica - Logica si ontologie.pdf

./Constantin Noica:
Constantin Noica - Concepte deschise in istoria filozofiei.pdf
Constantin Noica - Cuvint impreuna despre rostirea romaneasca.pdf
Constantin Noica - Despartirea de Goethe.pdf
Constantin Noica - Devenirea intru fiinta. Scrisori despre logica lui Hermes.pdf
Constantin Noica - Jurnal de idei.pdf
Constantin Noica - Jurnal filozofic.pdf
Constantin Noica - Manuscrisele de la Campulung.pdf
Constantin Noica - Mathesis sau bucruriile simple.pdf
Constantin Noica - Pagini despre sufletul romanesc.pdf
Constantin Noica - Povestiri despre om.pdf
Constantin Noica - Schita pentru istoria lui Cum e cu putinta ceva nou.pdf
Constantin Noica - Sentimentul romanesc al fiintei.pdf

./Constantin Salavastru:
Constantin Salavastru - Mic tratat de oratorie.pdf

./Constantin Stoenescu & Gheorghe Stefanov & Mircea Flonta:
Constantin Stoenescu & Gheorghe Stefanov & Mircea Flonta - Teoria cunoasterii.pdf

./Corina Benea:
Corina Benea - Modelarea explicatiei stiintifice.pdf

./Cornel Popa:
Cornel Popa - Logica si metalogica, vol. 1.pdf
Cornel Popa - Logica si metalogica, vol. 2.pdf

./Cristian Badilita:
Cristian Badilita - Convorbiri cu Schopenhauer.pdf
Cristian Badilita - Miturile lui Platon.pdf
Cristian Badilita - Socrate omul.pdf

./Cristian Ciocan:
Cristian Ciocan - Moribundus sum.pdf

./Cristian Ciocan & Dan Lazea:
Cristian Ciocan & Dan Lazea - Intentionalitatea de la Plotin la Levinas.pdf

./Cristian Iftode:
Cristian Iftode - Filosofia ca mod de viata.pdf
```

